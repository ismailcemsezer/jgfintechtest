using LiteDB;

namespace JG.FinTechTest.Dal
{
    public interface ILiteDbContext
    {
        LiteDatabase Database { get; }
    }
}