namespace JG.FinTechTest.Dal.Repositories
{
    public interface IGiftAidRepository
    {
        int Create(DonationDetailsModel donationDetails);
    }
}