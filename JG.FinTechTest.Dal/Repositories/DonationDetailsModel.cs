namespace JG.FinTechTest.Dal.Repositories
{
    public class DonationDetailsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PostCode { get; set; }
        public decimal DonationAmount { get; set; }
    }
}