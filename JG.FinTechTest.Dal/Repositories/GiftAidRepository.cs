namespace JG.FinTechTest.Dal.Repositories
{
    public class GiftAidRepository : IGiftAidRepository
    {
        private readonly ILiteDbContext _dbContext;

        public GiftAidRepository(ILiteDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public int Create(DonationDetailsModel donationDetails)
        {
            var giftAids = _dbContext.Database.GetCollection<DonationDetailsModel>("GiftAid");
            var bsonValue = giftAids.Insert(donationDetails);
            return bsonValue.AsInt32;
        }
    }
}