using System.IO;
using LiteDB;

namespace JG.FinTechTest.Dal
{
    public class LiteDbContext : ILiteDbContext
    {
        public LiteDatabase Database { get; }

        public LiteDbContext()
        {
            Database = new LiteDatabase(new MemoryStream());
        }
    }
}