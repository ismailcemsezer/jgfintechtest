using JG.FinTechTest.Dal.Repositories;

namespace JG.FinTechTest.Domain
{
    public interface IDonationService
    {
        int StoreDonationDetails(DonationDetailsModel donationDetails);
    }
}