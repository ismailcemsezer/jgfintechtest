using JG.FinTechTest.Dal.Repositories;

namespace JG.FinTechTest.Domain
{
    public class DonationService : IDonationService
    {
        private readonly IGiftAidRepository _giftAidRepository;
        
        public DonationService(IGiftAidRepository giftAidRepository)
        {
            _giftAidRepository = giftAidRepository;
        }

        public int StoreDonationDetails(DonationDetailsModel donationDetails)
        {
            return _giftAidRepository.Create(donationDetails);
        }
    }
}