using Microsoft.Extensions.Options;

namespace JG.FinTechTest.Domain
{
    public class GiftAidCalculator : IGiftAidCalculator
    {
        private readonly decimal _taxRate;

        public GiftAidCalculator(IOptions<TaxSettings> taxSettings)
        {
            _taxRate = taxSettings.Value.Rate;
        }
        public decimal CalculateGiftAid(decimal donationAmount)
        {
            if (donationAmount <= 0)
                return 0;
            
            return donationAmount * (_taxRate / (100 - _taxRate));
        }
    }
}