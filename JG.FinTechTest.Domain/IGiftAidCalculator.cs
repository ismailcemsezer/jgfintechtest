namespace JG.FinTechTest.Domain
{
    public interface IGiftAidCalculator
    {
        decimal CalculateGiftAid(decimal donationAmount);
    }
}