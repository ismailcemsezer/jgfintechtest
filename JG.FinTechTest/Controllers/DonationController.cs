using JG.FinTechTest.ApiContracts;
using JG.FinTechTest.Dal.Repositories;
using JG.FinTechTest.Domain;
using JG.FinTechTest.Validators;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JG.FinTechTest.Controllers
{
    [Route("api/donations")]
    [ApiController]
    public class DonationController : ControllerBase
    {
        private readonly IDonationService _donationService;
        private readonly IGiftAidCalculator _giftAidCalculator;
        private readonly IDonationValidator _donationValidator;

        public DonationController(IDonationService donationService, IGiftAidCalculator giftAidCalculator, IDonationValidator donationValidator)
        {
            _donationService = donationService;
            _giftAidCalculator = giftAidCalculator;
            _donationValidator = donationValidator;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult SaveGiftAid([FromBody] DonationDetails donationDetails)
        {
            var (isValid, errorMessage) = _donationValidator.ValidateDonationDetails(donationDetails);

            if (!isValid)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    Error = errorMessage
                });
            }

            var donationDetailsModel = new DonationDetailsModel
            {
                Name = donationDetails.Name,
                DonationAmount = donationDetails.DonationAmount,
                PostCode = donationDetails.PostCode
            };

            return new CreatedResult("", new DonationResponse
            {
                Id = _donationService.StoreDonationDetails(donationDetailsModel),
                GiftAidAmount = _giftAidCalculator.CalculateGiftAid(donationDetails.DonationAmount)
            });
        }
    }
}