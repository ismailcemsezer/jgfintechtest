﻿using System.ComponentModel.DataAnnotations;
using JG.FinTechTest.ApiContracts;
using JG.FinTechTest.Domain;
using JG.FinTechTest.Validators;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JG.FinTechTest.Controllers
{
    [Route("api/giftaid")]
    [ApiController]
    public class GiftAidController : ControllerBase
    {
        private readonly IGiftAidCalculator _giftAidCalculator;
        private readonly IDonationValidator _donationValidator;

        public GiftAidController(IGiftAidCalculator giftAidCalculator, IDonationValidator donationValidator)
        {
            _giftAidCalculator = giftAidCalculator;
            _donationValidator = donationValidator;
        }
        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetGiftAid([Required] decimal amount)
        {
            var (isValid, errorMessage) = _donationValidator.ValidateDonationAmount(amount);

            if (!isValid)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    Error = errorMessage
                });
            }

            return new OkObjectResult(new GiftAidResponse
            {
                DonationAmount = amount,
                GiftAidAmount = _giftAidCalculator.CalculateGiftAid(amount)
            });
        }
    }
}