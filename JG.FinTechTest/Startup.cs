﻿using JG.FinTechTest.Dal;
using JG.FinTechTest.Dal.Repositories;
using JG.FinTechTest.Domain;
using JG.FinTechTest.Validators;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace JG.FinTechTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.Configure<TaxSettings>(Configuration.GetSection("TaxSettings"));
            services.AddScoped<IGiftAidRepository, GiftAidRepository>();
            services.AddScoped<IDonationService, DonationService>();
            services.AddScoped<IGiftAidCalculator, GiftAidCalculator>();
            services.AddScoped<IDonationValidator, DonationValidator>();
            services.AddSingleton<ILiteDbContext, LiteDbContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
