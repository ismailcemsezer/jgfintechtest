namespace JG.FinTechTest.ApiContracts
{
    public class DonationDetails
    {
        public string Name { get; set; }
        public string PostCode { get; set; }
        public decimal DonationAmount { get; set; }
    }
}