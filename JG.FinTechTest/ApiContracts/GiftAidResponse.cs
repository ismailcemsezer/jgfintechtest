namespace JG.FinTechTest.ApiContracts
{
    public class GiftAidResponse
    {
        public decimal DonationAmount { get; set; }
        public decimal GiftAidAmount { get; set; }
    }
}