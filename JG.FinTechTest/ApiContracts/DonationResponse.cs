namespace JG.FinTechTest.ApiContracts
{
    public class DonationResponse
    {
        public int Id { get; set; }
        public decimal GiftAidAmount { get; set; }
    }
}