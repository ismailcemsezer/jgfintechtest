namespace JG.FinTechTest.ApiContracts
{
    public class ErrorResponse
    {
        public string Error { get; set; }
    }
}