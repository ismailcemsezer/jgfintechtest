using JG.FinTechTest.ApiContracts;

namespace JG.FinTechTest.Validators
{
    public interface IDonationValidator
    {
        (bool isValid, string ErrorMessage) ValidateDonationDetails(DonationDetails donationDetails);
        (bool isValid, string ErrorMessage) ValidateDonationAmount(decimal donationAmount);
    }
}