using JG.FinTechTest.ApiContracts;

namespace JG.FinTechTest.Validators
{
    public class DonationValidator : IDonationValidator
    {
        public (bool isValid, string ErrorMessage) ValidateDonationDetails(DonationDetails donationDetails)
        {
            var donationAmountValidation = ValidateDonationAmount(donationDetails.DonationAmount);

            if (!donationAmountValidation.isValid)
            {
                return donationAmountValidation;
            }

            if (string.IsNullOrEmpty(donationDetails.PostCode))
            {
                return (false, "Post code is either null or empty");
            }

            if (string.IsNullOrEmpty(donationDetails.Name))
            {
                return (false, "Name is either null or empty");
            }

            return (true, string.Empty);
        }

        public (bool isValid, string ErrorMessage) ValidateDonationAmount(decimal donationAmount)
        {
            if (donationAmount < 2.00m)
            {
                return (false, "Minimum donation amount is £2.00");
            }

            if (donationAmount > 100000.00m)
            {
                return (false, "Maximum donation amount is £100,000.00");
            }

            return (true, string.Empty);
        }
    }
}