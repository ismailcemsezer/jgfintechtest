using JG.FinTechTest.ApiContracts;
using JG.FinTechTest.Controllers;
using JG.FinTechTest.Dal.Repositories;
using JG.FinTechTest.Domain;
using JG.FinTechTest.Tests.Builders;
using JG.FinTechTest.Validators;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace JG.FinTechTest.Tests
{
    public class DonationControllerTests
    {
        private Mock<IGiftAidCalculator> _giftAidCalculator;
        private Mock<IDonationService> _donationService;
        private Mock<IDonationValidator> _donationValidator;

        [SetUp]
        public void Setup()
        {
            _giftAidCalculator = new Mock<IGiftAidCalculator>();
            _donationService = new Mock<IDonationService>();
            _donationValidator = new Mock<IDonationValidator>();
        }

        [Test]
        public void SaveGiftAid_ShouldReturnErrorResponse_WhenDonationIsNotValid()
        {
            // Arrange
            const decimal donationAmount = 0;
            var donationDetails = new DonationDetailsBuilder()
                .WithPostCode("AB12CD")
                .WithName(null)
                .WithDonationAmount(donationAmount)
                .Build();

            _donationValidator.Setup(v => v.ValidateDonationDetails(donationDetails)).Returns((false, "Error message"));

            var giftAidController = new DonationController(_donationService.Object, _giftAidCalculator.Object, _donationValidator.Object);

            // Act
            var giftAidResponse = giftAidController.SaveGiftAid(donationDetails);

            // Assert
            Assert.IsInstanceOf<BadRequestObjectResult>(giftAidResponse);
            Assert.IsInstanceOf<ErrorResponse>(((BadRequestObjectResult) giftAidResponse).Value);
            var error = (ErrorResponse) ((BadRequestObjectResult) giftAidResponse).Value;
            Assert.AreEqual("Error message", error.Error);
        }

        [Test]
        public void SaveGiftAid_ShouldReturnCreatedRecordId_WhenDonationAmountIsValid()
        {
            // Arrange
            const int donationId = 1;
            const decimal donationAmount = 1000m;
            const decimal giftAid = 25.1250m;
            
            var donationDetails = new DonationDetailsBuilder()
                .WithPostCode("AB12CD")
                .WithName("John Smith")
                .WithDonationAmount(donationAmount)
                .Build();
            
            _donationValidator.Setup(v => v.ValidateDonationDetails(donationDetails)).Returns((true, ""));
            _donationService.Setup(s => s.StoreDonationDetails(It.IsAny<DonationDetailsModel>())).Returns(donationId);
            _giftAidCalculator.Setup(c => c.CalculateGiftAid(donationAmount)).Returns(giftAid);
            var giftAidController = new DonationController(_donationService.Object, _giftAidCalculator.Object, _donationValidator.Object);

            // Act
            var giftAidResponse = giftAidController.SaveGiftAid(donationDetails);
            
            // Assert
            Assert.IsInstanceOf<CreatedResult>(giftAidResponse);
            Assert.IsInstanceOf<DonationResponse>(((CreatedResult) giftAidResponse).Value);
            var response = (DonationResponse) ((CreatedResult) giftAidResponse).Value;
            Assert.AreEqual(donationId, response.Id);
            Assert.AreEqual(giftAid, response.GiftAidAmount);
        }
    }
}