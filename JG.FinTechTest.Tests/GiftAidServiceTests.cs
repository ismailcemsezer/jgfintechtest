using JG.FinTechTest.Dal.Repositories;
using JG.FinTechTest.Domain;
using Moq;
using NUnit.Framework;

namespace JG.FinTechTest.Tests
{
    public class GiftAidServiceTests
    {
        private Mock<IGiftAidRepository> _giftAidRepository;
        private const decimal TaxRate = 20;
        
        [SetUp]
        public void Setup()
        {
            _giftAidRepository = new Mock<IGiftAidRepository>();
        }

        [Test]
        public void StoreGiftAid_ShouldCreateGiftAidInDatabase()
        {
            // Arrange
            var donationDetails = new DonationDetailsModel();
            var giftAidService = new DonationService(_giftAidRepository.Object);

            // Act
            giftAidService.StoreDonationDetails(donationDetails);

            // Assert
            _giftAidRepository.Verify(s => s.Create(donationDetails), Times.Once);
        }
    }
}