using JG.FinTechTest.ApiContracts;

namespace JG.FinTechTest.Tests.Builders
{
    public class DonationDetailsBuilder
    {
        private readonly DonationDetails _donationDetails = new DonationDetails();
        
        private string _name;
        private decimal _donationAmount;
        private string _postCode;

        public DonationDetails Build()
        {
            _donationDetails.Name = _name;
            _donationDetails.DonationAmount = _donationAmount;
            _donationDetails.PostCode = _postCode;
            return _donationDetails;
        }

        public DonationDetailsBuilder WithName(string name)
        { 
            _name = name;
            return this;
        }

        public DonationDetailsBuilder WithDonationAmount(decimal donationAmount)
        {
            _donationAmount = donationAmount;
            return this;
        }

        public DonationDetailsBuilder WithPostCode(string postCode)
        {
            _postCode = postCode;
            return this;
        }
    }
}