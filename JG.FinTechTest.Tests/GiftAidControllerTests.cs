using JG.FinTechTest.ApiContracts;
using JG.FinTechTest.Controllers;
using JG.FinTechTest.Domain;
using JG.FinTechTest.Validators;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace JG.FinTechTest.Tests
{
    public class GiftAidControllerTests
    {
        private Mock<IGiftAidCalculator> _giftAidCalculator;
        private Mock<IDonationService> _donationService;
        private Mock<IDonationValidator> _donationValidator;

        [SetUp]
        public void Setup()
        {
            _giftAidCalculator = new Mock<IGiftAidCalculator>();
            _donationService = new Mock<IDonationService>();
            _donationValidator = new Mock<IDonationValidator>();
        }

        [Test]
        public void GetGiftAid_ShouldReturnErrorResponse_WhenDonationAmountIsNotValid()
        {
            // Arrange
            const decimal donationAmount = 1.99m;
            var giftAidController = new GiftAidController(_giftAidCalculator.Object, _donationValidator.Object);

            // Act
            _donationValidator.Setup(v => v.ValidateDonationAmount(donationAmount)).Returns((false, "Error message"));
            var giftAidResponse = giftAidController.GetGiftAid(donationAmount);

            // Assert
            Assert.IsInstanceOf<BadRequestObjectResult>(giftAidResponse);
            Assert.IsInstanceOf<ErrorResponse>(((BadRequestObjectResult) giftAidResponse).Value);
            var error = (ErrorResponse) ((BadRequestObjectResult) giftAidResponse).Value;
            Assert.AreEqual("Error message", error.Error);
        }
        
        [Test]
        public void GetGiftAid_ShouldReturnGiftAid_WhenDonationAmountIsValid()
        {
            // Arrange
            const decimal donationAmount = 100.50m;
            const decimal giftAid = 25.1250m;

            _donationValidator.Setup(v => v.ValidateDonationAmount(donationAmount)).Returns((true, ""));
            _giftAidCalculator.Setup(c => c.CalculateGiftAid(donationAmount)).Returns(giftAid);
            var giftAidController = new GiftAidController(_giftAidCalculator.Object, _donationValidator.Object);

            // Act
            var giftAidResponse = giftAidController.GetGiftAid(donationAmount);

            // Assert
            Assert.IsInstanceOf<OkObjectResult>(giftAidResponse);
            Assert.IsInstanceOf<GiftAidResponse>(((OkObjectResult) giftAidResponse).Value);
            var response = (GiftAidResponse) ((OkObjectResult) giftAidResponse).Value;
            Assert.AreEqual(giftAid, response.GiftAidAmount);
            Assert.AreEqual(donationAmount, response.DonationAmount);
        }

    }
}