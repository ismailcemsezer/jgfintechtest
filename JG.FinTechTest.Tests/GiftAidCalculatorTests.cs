using JG.FinTechTest.Domain;
using Microsoft.Extensions.Options;
using NUnit.Framework;

namespace JG.FinTechTest.Tests
{
    public class GiftAidCalculatorTests
    {
        private IOptions<TaxSettings> _taxSettings;
        private const decimal TaxRate = 20;
        private const decimal DonationAmount = 100.50m;

        [SetUp]
        public void Setup()
        {
            _taxSettings = Options.Create(new TaxSettings()
            {
                Rate = TaxRate
            });
        }

        [Test]
        public void CalculateGiftAid_ShouldReturnCorrectGiftAid()
        {
            // Arrange
            var giftAidService = new GiftAidCalculator(_taxSettings);

            // Act
            var giftAid = giftAidService.CalculateGiftAid(DonationAmount);

            // Assert
            Assert.AreEqual(25.1250m, giftAid);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-10.00)]
        public void CalculateGiftAid_ShouldReturnZero_WhenDonationAmountIsNegativeOrZero(decimal donationAmount)
        {
            // Arrange
            var giftAidService = new GiftAidCalculator(_taxSettings);

            // Act
            var giftAid = giftAidService.CalculateGiftAid(donationAmount);

            // Assert
            Assert.AreEqual(0, giftAid);
        }
    }
}