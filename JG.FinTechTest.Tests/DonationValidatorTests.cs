using JG.FinTechTest.Tests.Builders;
using JG.FinTechTest.Validators;
using NUnit.Framework;

namespace JG.FinTechTest.Tests
{
    public class DonationValidatorTests
    {
        [Test]
        public void ValidateDonationAmount_ShouldReturnError_WhenDonationAmountLessThanMinimum()
        {
            // Arrange
            const decimal donationAmount = 1.99m;
            var sut = new DonationValidator();

            // Act
            var (isValid, errorMessage) = sut.ValidateDonationAmount(donationAmount);

            // Assert
            Assert.False(isValid);
            Assert.AreEqual("Minimum donation amount is £2.00", errorMessage);
        }

        [Test]
        public void ValidateDonationAmount_ShouldReturnError_WhenDonationAmountMoreThanMaximum()
        {
            // Arrange
            const decimal donationAmount = 100000.1m;
            var sut = new DonationValidator();

            // Act
            var (isValid, errorMessage) = sut.ValidateDonationAmount(donationAmount);

            // Assert
            Assert.False(isValid);
            Assert.AreEqual("Maximum donation amount is £100,000.00", errorMessage);
        }
        
        [Test]
        public void ValidateDonationAmount_ShouldReturnSuccess_WhenDonationAmountIsValid()
        {
            // Arrange
            const decimal donationAmount = 1000.00m;
            var sut = new DonationValidator();

            // Act
            var (isValid, errorMessage) = sut.ValidateDonationAmount(donationAmount);

            // Assert
            Assert.True(isValid);
        }

        [Test]
        public void ValidateDonationDetails_ShouldReturnError_WhenNameIsNullOrEmpty()
        {
            // Arrange
            const decimal donationAmount = 1000.00m;
            var donationDetails = new DonationDetailsBuilder()
                .WithPostCode("AB12CD")
                .WithName(null)
                .WithDonationAmount(donationAmount)
                .Build();
            var sut = new DonationValidator();

            // Act
            var (isValid, errorMessage) = sut.ValidateDonationDetails(donationDetails);

            // Assert
            Assert.False(isValid);
            Assert.AreEqual("Name is either null or empty", errorMessage);
        }
        
        [Test]
        public void ValidateDonationDetails_ShouldReturnError_WhenPostCodeIsNullOrEmpty()
        {
            // Arrange
            const decimal donationAmount = 1000.00m;
            var donationDetails = new DonationDetailsBuilder()
                .WithPostCode(null)
                .WithName("John Smith")
                .WithDonationAmount(donationAmount)
                .Build();
            var sut = new DonationValidator();

            // Act
            var (isValid, errorMessage) = sut.ValidateDonationDetails(donationDetails);

            // Assert
            Assert.False(isValid);
            Assert.AreEqual("Post code is either null or empty", errorMessage);
        }
    }
}