##  Gift Aid Calculation Endpont

Endpoint to calculate and return gift aid for a given donation amount

GET request to `/api/giftaid?amount=[decimal]`

Returns 200 status with this body


```
{
    "donationAmount": [decimal],
    "giftAidAmount": [decimal]
}
```

##  Gift Aid Declarations Endpioint

Endpoint to store Gift Aid Declarations and return gift aid amount 

POST request to `/api/donations` with body


```
{  
  "PostCode" : [string],
  "Name" : [string],
  "DonationAmount" : [decimal],
}
```

Returns 

```
{
    "id": [int],
    "giftAidAmount": [decimal]
}
```